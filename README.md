# nrb-common-schemas

Home of some common schemas used within the [nrb](https://www.nationella-riktlinjer.se) domain.

## Usage

```
var nrbCommonSchemas = require('nrb-common-schemas');

console.log(nrbCommonSchemas.contributor1Schema);
```